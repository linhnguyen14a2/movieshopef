﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieShopEF
{
    [Table("Purchase")]
    public class Purchase
    {
        public int Id { get; set; }
        [Required]
        public int UserId { get; set; }

        [Required]
        public Guid PurchaseNumber { get; set; }

        [DataType("decimal(18,2")]
        public decimal? TotalPrice { get; set; }

        //[Required, DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Column(TypeName = "datetime2")]
        public DateTime? PurchaseDateTime { get; set; }
        [Required]
        public int MovieId { get; set; }
    }
}
