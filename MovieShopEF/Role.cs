﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieShopEF
{
    [Table("Role")]
    public class Role
    {
        public int ID { get; set; }

        [Required]
        [MaxLength(20)]
        public string? Name { get; set; }
    }
}
