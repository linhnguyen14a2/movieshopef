﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;



namespace MovieShopEF
{
    [Table("Genre")]
    public class Genre
    {

        public int ID { get; set; }

        [StringLength(64)]
        [Required]
        public string Name { get; set; }
    }
}
