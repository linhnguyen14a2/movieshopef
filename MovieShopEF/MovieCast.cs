﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieShopEF
{
    [Table("MovieCast")]
    public class MovieCast
    {
        [Key]
        public int MovieId { get; set; }

        [Required]
        public int CastId { get; set; }
        
        [Required]
        [StringLength(450)]
        public string Character { get; set; }

    }
}
