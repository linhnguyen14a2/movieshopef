﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieShopEF
{
    [Table("MovieGenre")]
    public class MovieGenre
    {
        [Key]
        public int MovieID { get; set; }
        
        [Required]
        public int GenreID { get; set; }
    }
}
