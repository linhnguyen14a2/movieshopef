﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieShopEF
{
    public class UserRole
    {
        [Key]
        public int UserID { get; set; }

        [Required]
        public int RoleID { get; set; }
    }
}
