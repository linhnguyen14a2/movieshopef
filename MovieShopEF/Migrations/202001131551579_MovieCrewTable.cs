namespace MovieShopEF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MovieCrewTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MovieCrew",
                c => new
                    {
                        MovieId = c.Int(nullable: false, identity: true),
                        CrewId = c.Int(nullable: false),
                        Department = c.String(nullable: false, maxLength: 128),
                        Job = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.MovieId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.MovieCrew");
        }
    }
}
