namespace MovieShopEF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MovieCastTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MovieCast",
                c => new
                    {
                        MovieId = c.Int(nullable: false, identity: true),
                        CastId = c.Int(nullable: false),
                        Character = c.String(nullable: false, maxLength: 450),
                    })
                .PrimaryKey(t => t.MovieId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.MovieCast");
        }
    }
}
