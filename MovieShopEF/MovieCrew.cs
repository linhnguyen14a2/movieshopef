﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieShopEF
{
    [Table("MovieCrew")]
    public class MovieCrew
    {
        [Key]
        public int MovieId { get; set; }
        [Required]
        public int CrewId { get; set; }
        [Required]
        [StringLength(128)]
        public string Department { get; set; }
        [Required]
        [StringLength(128)]
        public string Job { get; set; }
    }
}
