﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieShopEF
{
    [Table("Review")]
    public class Review
    {
        [Key]
        public int MovieId { get; set; }

        [Required]
        public int UserId { get; set; }

        [Required]
        [DataType("decimal(3,2")]
        public decimal Rating { get; set; }

        [MaxLength]
        public string ReviewText { get; set; }
    }
}
